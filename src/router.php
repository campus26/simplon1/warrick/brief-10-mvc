<?php



class ObjTest 
{

    public $routes;
    public $dbConnection;
    public $page;
    public $action;

    function __construct($routes, $dbConnection )
    {
        $this->routes = $routes;
        $this->dbConnection = $dbConnection;
        $this->page = getParam('p');
        $this->action = getParam('a');

        if ($this->page === NULL) {
            $this->page = DEFAULT_PAGE;
        }

        if ($this->action === NULL) {
            $this->action = DEFAULT_ACTION;
        }
    }

    private function doGet($routes, $dbConnection)
    {

        

        if (array_key_exists($this->page, $routes)) {
            $matched = false;
            foreach ($routes[$this->page] as $value) {
                if ($this->action === $value) {

                    $matched = true;
                    break;
                }
            }

            if ($matched) {

                $controllerName = $this->page;
                $controllerName = ucfirst($controllerName);
                $controllerName .= 'Controller';

                $controller = new $controllerName();
                $controller->{$this->action}($dbConnection);

                /*$pageFile = PAGE . $page . '.php';
                if (file_exists($pageFile)) {
                    include_once($pageFile);
                    $action($dbConnection);
                }*/
            } else {
                echo 'Error 404<br>Page not found';
            }
        } else {
            echo 'Error 404<br>Page not found';
        }
    }

    private function doPost($routes, $dbConnection)
    {



        if (array_key_exists($this->page, $routes)) {
            $matched = false;
            foreach ($routes[$this->page] as $value) {
                if ($this->action === $value) {

                    $matched = true;
                    break;
                }
            }

            if ($matched) {

                $controllerName = $this->page;
                $controllerName = ucfirst($controllerName);
                $controllerName .= 'Controller';

                $controller = new $controllerName();
                $controller->{$this->action}($dbConnection);

                /*$pageFile = PAGE . $page . '.php';
                if (file_exists($pageFile)) {
                    include_once($pageFile);
                    $action($dbConnection);
                }*/
            } else {
                echo 'Error 404<br>Page not found';
            }
        } else {
            echo 'Error 404<br>Page not found';
        }
    }

    public function init($routes, $dbConnection)
    {
        $httpMethod = httpMethod();

        switch ($httpMethod) {
            case 'GET':
                $this->doGet($routes, $dbConnection);
                break;

            case 'POST':
                $this->doPost($routes, $dbConnection);
                break;

            default:
                break;
        }
    }
}

